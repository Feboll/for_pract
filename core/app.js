'use strict';
(function () {
    // Declare app level module which depends on views, and components
    angular.module('myApp', [
            'ui.router', // Routing
            'ui.bootstrap', // Ui Bootstrap
            'ngSanitize' // ngSanitize
        ])
        .config(function ($stateProvider) {
            $stateProvider
                .state('app', {
                    abstract: true
                })
                .state('app.main', {
                    url: "/",
                    controller: 'TaskController'
                })
                .state('app.task', {
                    url: "/task/:part/:number",
                    controller: 'TaskController'
                })
        })
        .controller('TaskController', ['$http', '$stateParams', '$scope', '$rootScope',
            function ($http, $stateParams, $scope, $rootScope) {
                $rootScope.$on('$stateChangeStart',
                    function (event, toState, toParams, fromState, fromParams, options) {
                        if (tasks[toParams.part]) {
                            $scope.task = tasks[toParams.part][toParams.number];
                        } else {
                            $scope.task = {
                                task: ''
                            };
                        }
                    })
            }
        ]);
})();